'''
装饰器是闭包的应用，函数可作为参数传入
可扩展函数
'''

# 装饰器模版
def warpper(func):
    def inner(*args,**kwargs):
        # 执行被装饰函数之前的操作
        res = func(*args,**kwargs)
        # 执行被装饰函数之后的操作
        return res
    return inner






