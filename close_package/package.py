'''
闭包：一个在其他函数内部定义的函数，它可以访问其外部函数的局部变量，常用于创建带有特定状态的函数，或者用来封装一些不应该被外部直接访问的变量
闭包可以延长引用外部变量的生命周期，外部变量应该存储于堆中
'''

# 闭包定义
def outer_function():
    x = 10

    def inner_function(y):
        return x + y

    return inner_function


fun = outer_function()
count = fun(20)
print("count: ", count)
