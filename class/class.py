
#类,继承
class Animal(object):
    #构造函数
    def __init__(self,name,age,type,address):
        self.__name = name
        self.__age = age
        self.__type = type
        self.__address =address
    # 析构函数 对象销毁前执行
    def __del__(self):
        print("被销毁了")

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,name):
        self.__name = name

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        self.__age = age

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, type):
        self.__type = type

    @property
    def address(self):
        return self.__address

    @address.setter
    def address(self, address):
        self.__address = address


    def eat(self,food):
        print(self.__name,'喜欢吃的食物：',food)


    def __str__(self):
        return f"动物名称： {self.__name} ,年龄： {self.__age} , 品种： {self.__type} , 产地: {self.__address} "

#继承Animal
class Dog(Animal):

    def __init__(self, name, age, type, address):
        super().__init__(name, age, type, address)


if __name__ == '__main__':
    animal = Dog("毛毛", '2', '哈士奇', '中国')
    animal.eat("肉")

