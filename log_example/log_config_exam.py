'''
    日志配置文件
'''
import datetime
import os
import logging
import logging.config
import django

#根目录
media_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 修改成linux目录 : media_root = media_root.replace('\\', '/')
#日志文件存放目录
logging_dir = media_root + "/logs/"
if not os.path.exists(logging_dir):
    os.mkdir(logging_dir)

# EMAIL_HOST = 'your smtp'    # SMTP服务器
# EMAIL_HOST_USER = 'your email' # 邮箱名
# EMAIL_HOST_PASSWORD = 'your password'# 邮箱密码
# EMAIL_PORT = 25     # 发送邮件的端口
# EMAIL_USE_TLS = True    # 是否使用 TLS
# DEFAULT_FROM_EMAIL = 'your email'   # 默认的发件人
# ADMINS = [('John', 'John@gmail.com'), ('Peter', 'Peter@gmail.com')]  # 邮件接收人，可以有多个

def genLogDict():
    # 日志配置
    LOGGING1 = {
        'version': 1,
        'disable_existing_loggers': False,  # 是否禁止默认配置的记录器
        # 格式化器
        'formatters': {
            # 设置多种格式，根据需求选择适合的格式
            'standard': {
                # 日志级别 当前时间 调用日志输出函数的模块的文件名 调用日志输出函数的函数名  调用日志输出函数的语句所在的代码行  用户输出的消息
                'format': '%(levelname)s %(asctime)s %(filename)s %(funcName)s %(lineno)d : %(message)s'
            },
            'simple': {
                'format': '%(levelname)s > %(message)s'
            },
        },
        # 过滤器
        'filters': {
            'require_debug_true': { # 当 DEBUG 为 True 时，传递记录
                '()': 'django.utils.log.RequireDebugTrue',
            },
        },
        # 处理器
        'handlers': {
            # 输出控制台
            'console': {
                'level': 'INFO',  #日志级别
                #'filters': ['require_debug_true'],  #过滤器
                'class': 'logging.StreamHandler',
                'formatter': 'simple'   # 日志格式选择
            },
            # 输出文件
            'file_handler': {
                 'level': 'INFO',
                 'class': 'logging.handlers.TimedRotatingFileHandler',
                 'filename': '%s/%s.log' % (logging_dir, datetime.datetime.today().date()),  #具体日志文件的名字
                 'formatter':'standard',
                 'when': 'MIDNIGHT', # 每天凌晨切分
                 'backupCount': 7  # 保存 30 天
            },
            # 发送邮件
            # 'mail_admins': {
            #     'level': 'ERROR',
            #     'class': 'django.utils.log.AdminEmailHandler',
            #     'formatter':'standard'
            # },
        },
        # 日志记录器 日志分配到哪个handlers中
        'loggers': {
            'your_app_name': {	# 后面导入时logging.getLogger使用的app_name
                'handlers': ['console'],  # 处理器选择
                'level':'DEBUG',
                'propagate': False,  # 日志记录会向上传播到他的父节点
            },
            'django.request': {  #如果要将get,post请求同样写入到日志文件中，则这个触发器的名字必须叫django,然后写到handler中
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': False,
            },
        }
    }

    return LOGGING1

#使用日志直接导入该模块的该函数
def initLogConf():
    logDict = genLogDict()
    logging.config.dictConfig(logDict)
    #修复Django BUG，参数value为配置文件名称
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "log_config_exam")
    django.setup()

