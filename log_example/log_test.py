import logging

'''
logger（记录器）：提供我们记录日志的方法；
handler（处理器）：选择日志的输出地方，如：控制台，文件，邮件发送等，一个logger可以添加多个handler；
filter（过滤器）：是给用户提供更加细致的控制日志的输出内容；
formater（格式化器）：用户格式化输出日志的信息。
'''

# 简单使用
logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
logger.info('This is a log info')
logger.debug('This is a log debug')
logger.warning('This is a log warning')

# 开发中使用 引用配置日志
from log_example.log_config_exam import initLogConf

initLogConf()
log = logging.getLogger("your_app_name")  # 自定义日志记录器名称


log = logging.getLogger("log_info")



if __name__ in '__main__':
    try:
        a = 1 / 0
    except Exception as e:
        log.error(str(e))  # 将异常写入日志
