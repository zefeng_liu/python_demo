'''
本模块介绍锁
'''
import threading
import time


num = 100


# 互斥锁（Mutexes）
# 互斥锁是最基本的锁类型，它确保只有一个线程可以同时访问共享资源。threading模块提供了Lock类来实现互斥锁。

# 锁对象
lock = threading.Lock()


def critical_section():
    # 获取锁
    lock.acquire()
    try:
        # 执行临界区代码
        global num
        num -= 1
        print(threading.current_thread().getName()," ,num: ",num)
    finally:
        # 释放锁
        lock.release()

for i in range(10):
    thread = threading.Thread(target=critical_section)
    # thread.start()





# 可重入锁（RLocks）
# 可重入锁（ReentrantLocks）允许同一个线程多次获取锁。递归函数

# 可重入锁对象
rlock = threading.RLock()


def nested_critical_section(depth):
    with rlock:  # 获取锁
        print(f"Entering recursive_function with depth: {depth}")
        if depth > 0:
            # 递归调用函数，同一个线程会再次尝试获取锁
            nested_critical_section(depth - 1)
        else:
            print(f"Exiting recursive_function with depth: {depth}")


thread = threading.Thread(target=nested_critical_section, args=(5,))
# thread.start()


# 信号量（Semaphores）
# 信号量用于限制对共享资源的同时访问数量。threading模块的Semaphore类提供了这个功能。

semaphore = threading.Semaphore(2)


def limited_access():
    # 尝试获取信号量
    semaphore.acquire()
    try:
        # 执行需要限制访问的操作
        print("Thread is accessing a limited resource")
        time.sleep(2)
    finally:
        # 释放信号量
        semaphore.release()

for i in range(10):
    thread = threading.Thread(target=limited_access)
    # thread.start()


# 事件（Events）
# 事件用于线程间的信号通知。其他线程等待事件状态的改变，修改事件状态通知其他线程运行。 其他线程运行到指定位置等待指定指定事件的发生后再运行。threading模块的Event类提供了这个功能。

# 事件对象
event = threading.Event()


def wait_for_event():
    print(f"{threading.current_thread().getName()} is waiting for the event.")
    event.wait()  # 等待事件被设置
    print(f"{threading.current_thread().getName()} was notified and is now doing work.")


for i in range(10):
    thread = threading.Thread(target=wait_for_event)
    thread.start()

time.sleep(10)
print("等我做完某些事情，你们再运行")
# 设置状态,唤醒阻塞线程
event.set()


# 条件变量（Condition Variables）
# 条件变量用于线程间基于条件的同步。它可以让一个线程等待某个条件为真，或者通知其他线程条件已经改变。threading模块的Condition类提供了这个功能。


condition = threading.Condition()


def wait_for_condition():
    with condition:
        # 检查条件是否满足，如果不满足则等待
        if not condition.wait_condition_met():
            condition.wait(timeout=1)  # 等待最多1秒钟
        print("条件已满足，执行相关操作...")

def set_condition():
    with condition:
        condition.condition_met = True  # 设置条件为满足状态
        condition.notify()  # 唤醒等待的线程

thread1 = threading.Thread(target=wait_for_condition)
thread2 = threading.Thread(target=set_condition)

thread1.start()
thread2.start()



# 读写锁（Read - WriteLocks）
# 读写锁允许多个线程同时读取共享资源，但只允许一个线程写入。threading模块的RLock类提供了类似的功能，但不是专门针对读写操作设计的。对于读写锁，可以使用rwlock模块中的RWLock类。

from rwlock import RWLock

lock = RWLock()


def reader():
    lock.acquire_read()
    try:
        # 读取操作
        print("Reader is accessing the shared resource")
    finally:
        lock.release_read()


def writer():
    lock.acquire_write()
    try:
        # 写入操作
        print("Writer is modifying the shared resource")
    finally:
        lock.release_write()




if __name__ in '__main__':
    pass




