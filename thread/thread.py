'''
本模块介绍线程和线程池
'''
import threading
from time import sleep
import concurrent.futures

# 任务函数
def worker(thread_name):
    print(f"Thread {thread_name} is starting.")
    sleep(1)
    print(f"Thread {thread_name} is finishing.")

# 任务函数
def task(thread_name):
    print(f"Thread {thread_name} is starting.")


# 创建线程，设置线程名，传入参数
thread = threading.Thread(target=worker,name='thread_1',args=("thread_1",))
# 启动线程
thread.start()


# 自定义线程,重写__init__和run方法
class MyThread(threading.Thread):

    def __init__(self):
        super().__init__()

    def run(self):
        task(threading.current_thread().getName())

MyThread.start()



# 线程池
# 线程池创建
with concurrent.futures.ThreadPoolExecutor(max_workers=10, thread_name_prefix="thread_") as pool_executor:
    for i in range(5):
        try:
            # 任务提交
            future = pool_executor.submit(worker, i)
            # 设置超时时间
            result = future.result(timeout=2)
            print(result)
        except:
            pass

    try:
        # 批量任务提交
        results = pool_executor.map(task, range(5))
        for result in results:
            print(result)
    except:
        pass

    # 销毁线程池,设置wait=False参数会立即关闭线程池
    pool_executor.shutdown()





if __name__ in '__main__':
    # 创建线程，设置线程名，传入参数
    thread = threading.Thread(target=worker,name='thread_1',args=("thread_1",))
    # 启动线程
    thread.start()
    print("\n",threading.current_thread().getName())
    print("\nmain线程")
    # 等待线程结束
    thread.join()
    print("main线程结束")
    pass

