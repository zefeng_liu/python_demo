"""
本模块介绍函数
"""
import random


# 函数定义
def generate_code(source):
    code = ''
    for i in range(12):
        r = random.choice(source)
        code += r
    return code


# lambda表达式
sum_anno = lambda a, b: a + b

# global用法：函数内部声明为全局变量，可修改变量，同时外部可访问
flag = "global标识"


def change():
    global flag
    global bug
    flag = "修改global标识"
    bug = "外部可访问"


change()

print("flag: ", flag)
print("bug: ", bug)


# nonlocal用法：用于在嵌套函数中声明一个变量是来自外层（但不是最外层）作用域的变量

def outer_function():
    local_var = "I am a local variable in outer function"

    def inner_function():
        # 修改嵌套作用域中的局部变量
        nonlocal local_var
        local_var = "Modified local variable"

    inner_function()
    print(local_var)


outer_function()


# 可变参数
def my_function(*args):
    for arg in args:
        print(arg)


my_function(1, 2, 3, 'a', 'b')

# 关键字的可变参数
def my_function(**kwargs):
    for key, value in kwargs.items():
        print(f"{key}: {value}")


my_function(name='John', age=30, city='New York')


# 下方案例 图书借阅系统
# 登录标识
is_login = False

# 登录
def lib_login(name, password):
    # 查阅数据库，用户和密码是否正确
    if name == 'admin' and password == '123456':
        # global 函数内部声明为全局变量，可修改变量，同时外部可访问
        global is_login
        is_login = True
        print('登录成功！！')
    else:
        print('登录失败，请重新登录')

# 借阅图书
def bro_book(book):
    while True:
        # 先判断是否登录
        if is_login:
            print("""
                    请稍等.....
                    正查找相关书籍.....
                    已查到.....
                    正在出库.....
            """)
            print("借阅成功 ： ", book)
            break
        else:
            print("未登录，请先登录")
            name = input("输入账号：")
            password = input("输入密码：")
            lib_login(name, password)


if __name__ in '__main__':
    # 函数调用
    source = 'QWEYRYUIOOPAFJLSLFBZBCFJKVNLANdsfdfagpirqgjlnbxbjljn'
    result = generate_code(source)
    print("result: ", result)

    # lambda调用
    result = sum_anno(10, 20)
    print("result: ", result)

    # 检测图书借阅系统
    # bro_book("完美世界")
