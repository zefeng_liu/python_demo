#-*-coding: UTF-8 -*-
import requests
import json

def test():
    nested_dict = {
        "people": {
            "person1": {
                "name": "Alice",
                "age": 30,
                "address": {
                    "street": "123 Main St",
                    "city": "New York",
                    "zip": "10001"
                }
            },
            "person2": {
                "name": "Bob",
                "age": 25,
                "address": {
                    "street": "456 Elm St",
                    "city": "Los Angeles",
                    "zip": "90001"
                },
                "hobbies": ["reading", "gaming"]
            }
        }
    }

    # 将多层嵌套字典转换为JSON格式的字符串
    json_string = json.dumps(nested_dict, indent=4)

    # 打印JSON字符串
    print(json_string)



    query= {
        "eid": "4ffcc3b9-43de-4009-be78-c598387f4a8e",
        "stock_percent": {"$gt":"0"},
        "is_history": 0,
        "u_tags": 0,
        "stock_name": {"$ne":"其他股东"}
    }
    nested_dict = {
        "person1": {
            "name": "Alice",
            "age": 30,
            "city": "New York"
        },
        "person2": {
            "name": "Bob",
            "age": 25,
            "city": "Los Angeles"
        }
    }

    # 将嵌套字典转换为JSON格式的字符串
    json_string = json.dumps(nested_dict, indent=4)  # indent参数用于格式化输出，使其更易于阅读

    # 打印JSON字符串
    print(json_string)

    param = {
        "query": {
            "eid": "4ffcc3b9-43de-4009-be78-c598387f4a8e",
            "stock_percent": {"$gt":"0"},
            "is_history": 0,
            "u_tags": 0,
            "stock_name": {"$ne":"其他股东"}
        },
        "fields": {
            "id": 1,
            "eid": 1,
            "seq_no": 1,
            "stock_name": 1,
            "stock_num": 1,
            "real_capi": 1,
            "should_capi_conv": 1,
            "should_capi": 1,
            "stock_percent": 1,
            "stock_id": 1,
            "stock_type": 1,
            "stock_type_code": 1,
            "share_type": 1
        },
        "orderby": {"stock_percent": -1},
        "num_to_return": 10
    }

    params = json.dumps(param,indent=4)
    return params


num = 1
if 0 < num <= 5:
    print(f"{num}\n")
elif num > 5:
    print(f"{num}\n")
else:
    print(f"{num}\n")















if  __name__ == '__main__':
    data = test()
    print(data)