#-*-coding: UTF-8 -*-
import requests
import json


def post_api():
    request_url = "http://business-proxy-internal.qixin.com/qxbproxy/get_info_list?info_name=BEST_STOCK_INFO&from=DataMining"
    param = {
        "query": {
            "eid": "4ffcc3b9-43de-4009-be78-c598387f4a8e",
            "stock_percent": {"$gt": "0"},
            "is_history":0,
            "u_tags": 0,
            "stock_name": {"$ne": "其他股东"}
        },
        "fields": {
            "id":1,
            "eid":1,
            "seq_no":1,
            "stock_name":1,
            "stock_num":1,
            "real_capi":1,
            "should_capi_conv":1,
            "should_capi":1,
            "stock_percent":1,
            "stock_id":1,
            "stock_type":1,
            "stock_type_code":1,
            "share_type":1
        },
        "orderby": {"stock_percent": -1},
        "num_to_return": 10
    }
    #请求头设置
    header = {'content-type': 'application/json;charset="UTF-8"'}
    #请求体转为json
    params = json.dumps(param)
    response = requests.post(url=request_url,data=params,headers=header)

    if response.status_code != 200:
       raise ConnectionError(f'{request_url} status code is {response.status_code}')

    #返回体转为字典
    response = json.loads(response.text)

    if 'status' not in response.keys() and response['status'] != '1':
        raise ValueError(f'{request_url} status异常')

    if 'data' not in response.keys():
        raise ValueError(f'{request_url} 返回参数不含data')

    data = response['data']

    return data


if  __name__ == '__main__':
    data = post_api()
    print(data)

