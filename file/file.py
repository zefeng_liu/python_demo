"""
本模块介绍如何对文件操作
"""
import os

#对文件读写操作
def readline():
    #with 相当于close()方法 会自动关闭流
    with open(r"in\wenzhang.txt",'r',encoding='utf-8') as f:
        #是否可读
        able = f.readable()
        print("是否可读：",able)
        #读取一行内容
        line = f.readline()
        while line:
            #剔除转换符
            line = line.replace('\n', '')
            print(line)
            line = f.readline()

def read():
    #with 相当于close()方法 会自动关闭流
    with open(r"in\wenzhang.txt",'r',encoding='utf-8') as f:
        #是否可读
        able = f.readable()
        print("是否可读：",able)
        #读取全部内容
        line = f.read()
        print(line)


def readlines():
    #with 相当于close()方法 会自动关闭流
    with open(r"in\wenzhang.txt",'r',encoding='utf-8') as f:
        #是否可读
        able = f.readable()
        print("是否可读：",able)
        ##一次读取所有内容并按行返回list
        line = f.readlines()
        print(line)

def write():
    # with 相当于close()方法 会自动关闭流
    with open(r"in\load.txt", 'w', encoding='utf-8') as f:
        #是否可写
        able = f.writable()
        print("是否可写：",able)
        line = "some data to be written to the file"
        #写一行
        f.write(line)

def writelines():
    # with 相当于close()方法 会自动关闭流
    with open(r"in\load.txt", 'w', encoding='utf-8') as f:
        # 是否可写
        able = f.writable()
        print("是否可写：", able)
        lines = ("some data to be written to the file\n","路上只我一个人，背着手踱着。\n","这一片天地好像是我的;我也像超出了平常旳自己，到了另一世界里。")
        #一次写入多行
        f.writelines(lines)



def learn_os():
    """
    注意，如果是读写文件的话，建议使用内置函数open()；
    如果是路径相关的操作，建议使用os的子模块os.path；
    如果要逐行读取多个文件，建议使用fileinput模块；
    要创建临时文件或路径，建议使用tempfile模块；
    要进行更高级的文件和路径操作则应当使用shutil模块。

    """
    pass



if __name__ in '__main__':
    pass
    readlines()
    # writeAppend()













































































