import datetime
import os
import logging
import logging.config
import django

# 创建日志目录
media_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logging_dir = media_root + "/logs/"
if not os.path.exists(logging_dir):
    os.mkdir(logging_dir)

# 日志配置
def log_config():
    log = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'format_1': {
                'format': '%(levelname)s %(asctime)s %(filename)s %(funcName)s %(lineno)d : %(message)s'
            },
            'format_2': {
                'format': '%(levelname)s > %(message)s'
            },
        },
        'filters': {
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
        },
        'handlers': {
            'console_output': {
                'level': 'INFO',
                'filters': ['require_debug_true'],
                'class': 'logging.StreamHandler',
                'formatter': 'format_1'
            },
            'file_output': {
                'level': 'INFO',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filename': '%s/%s.log' % (logging_dir, datetime.datetime.today().date()),
                'formatter': 'format_1',
                'when': 'MIDNIGHT',
                'backupCount': 7
            }
        },
        'loggers': {
            'log_info': {
                'handlers': ['console_output', 'file_output'],
                'level': 'INFO',
                'propagate': False,
            },
            'log_error': {
                'handlers': ['file_output'],
                'level': 'ERROR',
                'propagate': False,
            },
            'django.request': {
                'handlers': ['file_output'],
                'level': 'ERROR',
                'propagate': False,
            },
        }
    }
    return log


def init_log_conf():
    log = log_config()
    logging.config.dictConfig(log)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "log.log_online")
    django.setup()
