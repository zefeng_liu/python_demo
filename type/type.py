'''
    数据类型 Number(数字),String(字符串),List(列表),Tuple(元组),Dictionary(字典)
'''

counter = 100   # 赋值整型变量
miles = 1000.0  # 浮点型
name = "John"   # 字符串
a = b = c = 1   # 多个变量相同赋值
d, e, f = 1, 2, "john"   #多个变量不同赋值

# 列表
list1 = ['physics', 'chemistry', 1997, 2000]

# 元组，元素不可变
tup1 = ('physics', 'chemistry', 1997, 2000)
# 元组中只包含一个元素时，需要在元素后面添加逗号
tup2 = (1,)


# 字典
param = {
    "query": {
        "eid": "4ffcc3b9-43de-4009-be78-c598387f4a8e",
        "stock_percent": {"$gt": "0"},
        "is_history": 0,
        "u_tags": 0,
        "stock_name": {"$ne": "其他股东"}
    },
    "fields": {
        "id": 1,
        "eid": 1,
        "seq_no": 1,
        "stock_name": 1,
        "stock_num": 1,
        "real_capi": 1,
        "should_capi_conv": 1,
        "should_capi": 1,
        "stock_percent": 1,
        "stock_id": 1,
        "stock_type": 1,
        "stock_type_code": 1,
        "share_type": 1
    },
    "orderby": {"stock_percent": -1},
    "num_to_return": 10
}


# 集合
set1 = {1, 2, 3, 4}
set2 = set([4, 5, 6, 7])





