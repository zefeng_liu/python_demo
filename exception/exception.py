'''
本模块介绍异常处理，异常捕获，异常查看，全局异常处理
'''

import sys
import traceback
import log

# 异常捕获处理
try:
    print("可能出现异常代码")
except (Exception, IOError) as e:
    print("捕获到异常")
else:
    print("上面未出现异常执行下面代码")
finally:
    print("上面无论是否出现异常都要执行下面代码")


# 抛出异常
def throw():
    raise ValueError("参数异常")


# 记录异常信息
def writer_log():
    try:
        a = 4/0
    except Exception as e:
        # sys.exc_info()获取异常信息
        # 返回3元素元组， type（异常类型的名称）、value（捕获到的异常实例） 和 traceback
        exec_inf = sys.exc_info()
        # traceback模块获取具体的错误信息,介绍四种方法，前三种也可输入到日志文件，但不推荐，推荐第四种可结合日志输入日志
        traceback.print_tb(exec_inf[2],file=sys.stdout)
        traceback.print_exception(exec_inf[0], exec_inf[1], exec_inf[2], file=sys.stdout)
        traceback.print_exc(file=sys.stdout)
        log.log1.error(traceback.format_exc())

writer_log()


# 自定义异常类
class MyException(Exception):
    def __init__(self, code, message):
        super().__init__()
        self.code = code
        self.message = message

    def __str__(self):
        return f"{self.code} : {self.message}"


def test_my_exception(a, b):
    if b == 0:
        raise MyException(400, "Division by zero")
    else:
        return a / b

# test_my_exception(1,0)


# 全局异常处理 sys.excepthook是全局异常的处理钩子,
# 通过自定义异常处理函数替代sys.excepthook，达到对不同的异常的处理
def handleException(excType, excValue, tb):
    """
    :param excType:异常类型
    :param excValue:异常对象
    :param tb:异常的trace back
    :return:
    """
    if isinstance(excValue, MyException):
        return f"{400} : {'zero异常'}"
    elif isinstance(excValue, ValueError):
        return f"{300} : {'参数错误'}"
    else:
        return f"{500} : {'其他异常'}"

#直接将自定义异常处理函数替代，但要位于全局代码前，否则之前的错误不会被该函数处理
sys.excepthook = handleException



if __name__ in '__main__':

    test_my_exception(1, 0)

    pass